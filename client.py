#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

if len(sys.argv) != 6:
    sys.exit("Usage: client.py ip puerto REGISTER sip_address expires_value")

# Constantes. Dirección IP del servidor y contenido a enviar

SERVER = sys.argv[1]  # dirección ip
PORT = int(sys.argv[2])  # puerto
REGISTER = str(sys.argv[3:])
REGISTER = ' '.join(sys.argv[3:])
GMAIL = sys.argv[4]
EXPIRES = sys.argv[5]

if int(EXPIRES) < 0:
    sys.exit("El término de EXPIRES a introducir debe de ser positivo")

# if str(sys.argv[3]) != 'REGISTER':
    # sys.exit("Debe utilizarse el término REGISTER")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    LINE = "sip: " + str(GMAIL) + "SIP/2.0r" + " EXPIRES: " + EXPIRES
    print("Enviando:", LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")

