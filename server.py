#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dic = {}

    def register2json(self):
        """
        escribe en un fichero nuestro diccionario creado
        """
        json_file = "registered.json"
        with open(json_file, 'w') as file:
            json.dump(self.dic, file)

    def json2registered(self):
        """
        lee y comprueba si escoge el fichero o no
        """
        json_file = "registered.json"
        try:
            with open(json_file, 'r') as file2:
                self.dic = json.load(file2)
        except:
            pass
            print("no encuentra")

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        for line in self.rfile:
            info = self.rfile.read()

            print(line.decode('utf-8'))

            data = line.decode('utf-8').split()
            print("enviando", data)
            data1 = data[1]

            expiress = int(data[3])

            print('Cliente envia: ', data)

            sg = time.time() + expiress
            tim_actual = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
            tim = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(sg))
            IP = self.client_address[0]
            self.dic[data1] = "ADDRESS: " + IP + "EXPIRES: " + tim

            for client in self.dic.copy():
                if tim_actual >= self.dic[client][1]:
                    del self.dic[client]

            if expiress == '0':
                del self.dic[data1]
                print("SIP/2.0 200 OK")
            self.register2json()
            print(self.dic)


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")

